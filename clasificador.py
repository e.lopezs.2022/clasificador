#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

def es_correo_electronico(cadena):
    # Verifica si la cadena contiene el símbolo '@'
    if '@' in cadena:
        # Divide la cadena en dos partes en el símbolo '@'
        partes = cadena.split('@')
        if len(partes) == 2:
            nombre, dominio = partes
            # Verifica si hay caracteres a la izquierda y a la derecha del '@' y al menos un '.' en el dominio
            if nombre and dominio.count('.') >= 1:
                return True
    return False

def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def es_real(entrada):
    try:
        float(entrada)
        return True
    except ValueError:
        return False

def evaluar_entrada(string):
    if not string:
        return "No ha ingresado ningún string"
    elif es_correo_electronico(string):
        return "Es un correo electrónico."
    elif es_entero(string):
        return "Es un entero."
    elif es_real(string):
        return "Es un número real."
    else:
        return "No es ni un correo, ni un entero, ni un número real."

def main():
    # Verifica si se ha proporcionado al menos un argumento
    if len(sys.argv) < 2:
        print("Error: se espera al menos un argumento")
        sys.exit(1)

    # Obtiene el argumento desde la línea de comandos
    string = sys.argv[1]

    # Evalúa la entrada y muestra el resultado
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
